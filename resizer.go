package main

import (
    "errors"
    "fmt"
    "github.com/nfnt/resize"
    "image"
    "image/jpeg"
    "image/png"
    "log"
    "net/http"
    "strconv"
)

var client = http.Client{}

func resizeHandler(response http.ResponseWriter, request *http.Request) {
    sourceUri, err := getSourceUriFrom(request)
    if err != nil {
        http.Error(response, "Invalid source URI", 400)
        return
    }

    log.Printf("Requesting %s\n", sourceUri)

    sourceRequest, err := client.Get(sourceUri)
    if err != nil {
        http.Error(response, "Source image not found", 404)
        return
    }
    sourceImage, _, err := image.Decode(sourceRequest.Body)
    defer sourceRequest.Body.Close()

    if err != nil {
        http.Error(response, err.Error(), 400)
        return
    }

    width, height, err := getWidthAndHeightFrom(request)
    if err != nil {
        http.Error(response, err.Error(), 400)
        return
    }

    resized := resize.Resize(uint(width), uint(height), sourceImage, resize.NearestNeighbor)

    encodeAndRespondWithImage(resized, request, response)
}

func getSourceUriFrom(request *http.Request) (string, error) {
    if source := request.FormValue("source"); source != "" {
        return source, nil
    } else {
        return "", errors.New("'source' must be set")
    }
}
func getWidthAndHeightFrom(request *http.Request) (uint64, uint64, error) {

    width, err := strconv.ParseUint(request.FormValue("width"), 10, 0)
    if err != nil {
        return 0, 0, errors.New("Invalid width")
    }

    height, err := strconv.ParseUint(request.FormValue("height"), 10, 0)
    if err != nil {
        return 0, 0, errors.New("Invalid height")
    }

    return width, height, nil
}

func encodeAndRespondWithImage(image image.Image, request *http.Request, response http.ResponseWriter) {
    var format string = "jpg"

    if requestedFormat := request.FormValue("format"); requestedFormat != "" {
        format = requestedFormat
    }

    var err error
    switch format {
    case "jpg":
        {
            err = jpeg.Encode(response, image, nil)
        }
    case "png":
        {
            err = png.Encode(response, image)
        }
    default:
        {
            http.Error(response, "Invalid image media type", 400)
            return
        }
    }
    if err != nil {
        http.Error(response, err.Error(), 500)
        return
    }

    //response.Header().Set("Content-Length", fmt.Sprint(sourceRequest.ContentLength))
    response.Header().Set("Content-Type", fmt.Sprintf("image/%s", format))
}

func main() {
    fmt.Println("Starting server...")
    http.HandleFunc("/", resizeHandler)
    if err := http.ListenAndServe(":8080", nil); err != nil {
        panic(err)
    }
    fmt.Println("Done.")
}
